**Qu'est-ce que le CSV ?**

Le CSV (pour Comma Separated Values) est un type de fichier souvent utilisé dans des tableurs comme Calc ou Excel. 
Il n'a pas de spécification formelle. Le séparateur utilisé en France est la virgule tandis que dans le reste du monde entier utilise le point.
La première ligne du document dans un tableur correspond aux attributs et le resta aux valeurs. Un document au format CSV est souvent utilisé avec des
dictionnaires sur Python, le dictionnaire ayant le couple clé / valeur.

**Quel est mon projet ?**

Mon projet comporte un document au format CSV répertoriant l'intégralité des véhicules présents dans le jeu vidéo *Forza Horizon 3* sorti en 2016. Aussi,
des images des véhicules présents sont disponibles.

**En quoi consiste le programme écrit en Python ?**

Le programme que j'ai écrit est composé de plusieurs ensembles, afin d'éviter de se perdre dans le document en csv. Il y a quelques ensembles afin de trouver quelques véhicules en fonction de leurs informations. Les fonctions, quant à elles, permettent de trouver un véhicule avec des critères plus précis.

**Les ensembles du programme**

Il y a 7 ensembles dans le programme : un pour trouver tous les véhicules ayant un moteur V6, un autre pour répertorier tous les véhicules de 2008 ou plus, etc. On commence par la ligne['nom_modèle'] pour avoir l'ensemble des véhicles, puis on cherche dans notre document csv et on met le critère requis pour l'ensemble (ex: ['pays']).

**Les fonctions**

Les fonctions sont plus poussées que les ensembles, dans le sens où on va devoir faire des modifications à partir du document csv. Par exemple, pour la fonction voiture_plus_chere_pays, on cherche la voiture la plus chère en fonction du pays. Cependant, on va devoit chhanger la signature de la valeur. On va la transformer en float et ainsi retirer les virgules par un .replace (",",""). Dans certaines fonctions, on utilise également un .pop(), afin d'enlever un élément situé à une place défini et on le renvoie.


**Le document et les photos**

Un document csv est présent dans le menu master du projet, vous pourrez voir chaque caractéristique de chaque voiture du jeu et un répertoire Forza Horizon 3 véhicules a été créé afin de voir à quoi ressemble la voiture.