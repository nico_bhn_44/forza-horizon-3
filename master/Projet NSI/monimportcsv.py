import csv

def depuis_csv(nom_fichier_csv, separateur= ';', encodage='utf-8'):
    """
    En créant cette fonction, on va pouvoir ouvrir notre fichier en csv et créer une liste de dictionnaires.
    La première ligne du fichier contenant les attributs servira de première ligne dans le tableur.
    """
    
    lecteur = csv.DictReader(open(nom_fichier_csv, 'r', encoding=encodage), delimiter=separateur)
    return [dict(ligne) for ligne in lecteur]

def vers_csv():
    """
    Cette fonction permet d'exporter une liste de dictionnaires sous la forme d'un fichier csv.
    Le nom de la table sera rentré sous la forme d'un str.
    L'ordre des colonnes sera sous la forme d'une liste d'attributs. Voici un exemple ci-dessous.
    >>> vers_csv(Groupe1, 'newGroupe1', ordre_cols=['Nom', 'Anglais', 'Info', 'Maths'])
    """
    with open(nom_export + '.csv', 'w') as fic_csv:
        ecrit = csv.DictWriter(fic_csv, fieldnames=ordre_cols)
        ecrit.writeheader() #pour la 1ère ligne
        for ligne in nom_table:
            ecrit.writerow(ligne) #lignes ajoutées 1 à 1
    return None