#1 Les importations

import csv
from monimportcsv import depuis_csv, vers_csv
from typing import Dict, Set, List, Tuple

vehicules_Forza_Horizon_3 = depuis_csv(r'C:\Users\Nicolas\Documents\Test Projet\Forza_Horizon_3_raw.csv') # On insère le chemin du fichier et le r' devant la string permet de ne pas avoir de problème sue les caractères déchappement

#2 Des ensembles pour mieux se repérer dans ce fichier csv

# Ensemble des vehicules ayant un moteur V6
moteur_V6 = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if ligne['moteur'] == 'V6'
}

# Ensemble des vehicules ayant un IP de rang A
vehicule_IP_rang_A = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if 'A' in ligne['IP']
}

# Ensemble des vehicules français
vehicules_France = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if ligne['pays'] == 'France'
}

# Ensemble des vehicules japonais
vehicules_Japon = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if ligne['pays'] == 'Japon'
}

# Ensemble des vehicules datant de 2008 ou plus
vehicule_2008_ou_plus = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if ligne['annee'] >= '2008'
}

# Ensemble des vehicules inclus dans un DLC
vehicule_DLC = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if ligne['inclus_dans_un_DLC'] == '1'
}

# Ensemble des vehicules datant de 2013
vehicule_2013 = {
    ligne['nom_modele']
    for ligne in vehicules_Forza_Horizon_3
    if ligne['annee'] == '2013'
}

# Ensemble des vehicules ayant une valeur comprise entre 22,000 et 100,000 credits 
valeur_voiture = {
    ligne['nom_modele'] # On veut l'ensemble des noms des véhicules
    for ligne in vehicules_Forza_Horizon_3 # On cherche dans le document csv
    if (ligne['valeur'].replace(",", "")!="") and float(ligne['valeur'].replace(",", ""))>float(20000) and float(ligne['valeur'].replace(",",""))<float(100000)) # On retire les virgules (de ce style : 22,000 -> 22000)

#3 Quelques fonctions qui peuvent servir

def la_voiture_la_plus_chere(vehicules):
    """
    Renvoie la voiture la plus chere du jeu.
    La signature de la valeur en float.
    Les virgules sont remplaces 
    """
    voiture_resultat = vehicules[0]
    for une_voiture in vehicules:
        mon_prix = une_voiture['valeur'].replace(",", "") if une_voiture['valeur'] else 0
        if float(mon_prix) > float(voiture_resultat['valeur'].replace(",", "")):
            voiture_resultat = une_voiture
    return voiture_resultat
    
def trouve_voiture(nom : str):
    """
    Retourne le nom d'un vehicule.
    Commence par le premier vehicule du jeu.
    """
    return [voiture
            for voiture in vehicules_Forza_Horizon_3
            if voiture['nom_modele'] == nom][0]
            
def voiture_plus_chere_pays(liste_voiture):
    """
    Retourne la plus chere du jeu par pays.
    On change la signature de la valeur en float.
    On remplace les virgules
    """
    voiture_resultat = trouve_voiture(liste_voiture.pop())
    for nom_voiture in liste_voiture:
        une_voiture = trouve_voiture(nom_voiture)
        mon_prix = une_voiture['valeur'].replace(",", "") if une_voiture['valeur'] else 0
        if float(mon_prix) > float(voiture_resultat['valeur'].replace(",", "")):
            voiture_resultat = une_voiture
    return voiture_resultat
    
def performances_voiture(liste_voiture):
    """
    Renvoie le nom du vehicule avec l'IP le plus eleve du jeu.
    L'IP est defini comme un str.
    """
    voiture_IP_plus_eleve = trouve_voiture(liste_voiture.pop()) # On crée la variable voiture_IP_plus_eleve. Le .pop() retire un élément situé à une place précise dans un document et l'affiche
    for nom_voiture in liste_voiture: # On cherche le nom du véhicule
        la_voiture = trouve_voiture(nom_voiture) # On crée la variable la-voiture
        mon_IP = une_voiture['IP'] > str(voiture_IP_plus_eleve['IP']) # On crée la variable mon_IP
        if str(mon_IP) > str(la_voiture['IP']): # On compare l'IP actuel avec l'IP
            voiture_IP_plus_eleve = la_voiture # On fait une affectation
    return la_voiture # On retourne le résulat
    
    #4 Quelques exemples
    
    print("Ensemble des vehicules ayant un moteur V6")
    for voiture in moteur_V6:
        print(voiture)